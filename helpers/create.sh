#!/bin/bash -x

function generate_secret() {

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: ${1}-cluster
  labels:
    argocd.argoproj.io/secret-type: cluster
type: Opaque
stringData:
  name: ${1}-cluster
  server: "$(grep server /tmp/${1}/config | awk '{print $2}')"
  config: |
    {
      "tlsClientConfig": {
        "caData": "$(grep certificate-authority-data /tmp/${1}/config | awk '{print $2}')",
        "certData": "$(grep client-certificate-data /tmp/${1}/config | awk '{print $2}')",
        "keyData": "$(grep client-key-data /tmp/${1}/config | awk '{print $2}')"
      }
    }
EOF

}

generate_secret dev
generate_secret prod

helm repo add argo https://argoproj.github.io/argo-helm
helm upgrade --install argocd argo/argo-cd --values=apps/bootstrap/argocd/argocd-values.yaml

kubectl apply -k apps/base
